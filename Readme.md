# Hadley's presentations

This is some data analysis practices following the approach proposed by Hadley,
one of the creators of *tidyverse* collection of packages. In this repository
I try to follow (sometimes repeating) the methods and tools used to make data
exploration, analysis. Focus on reproducible ideas.

# License

Creative Commons license applies to this content.
